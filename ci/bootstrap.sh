#!/bin/bash

if [ ! -f /.dockerenv ]; then
  echo "Refusing to run CI commands outside of build environment."
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

prepare_ssh() {
  eval $(ssh-agent -s)
  mkdir -p ~/.ssh && chmod 700 ~/.ssh
  echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa
  ssh-add ~/.ssh/id_rsa
  ssh-keyscan git.ewscloud.com >> ~/.ssh/known_hosts 2>&1
}

copy_env() {
  ENV_FILE="$DIR/../.env"

  if [ ! -f "$ENV_FILE" ]; then
    cp "$DIR/.env" "$ENV_FILE"
  fi
}

main() {
  prepare_ssh
  copy_env
}

main
