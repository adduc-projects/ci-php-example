#!/bin/sh

if [ ! -f /.dockerenv ]; then
  echo "Refusing to run CI commands outside of build environment."
  exit 1
fi

catch() {
    OUTPUT=$($@ 2>&1)
    EXIT_CODE=$?
    if [ $EXIT_CODE -ne 0 ]; then
        echo "$OUTPUT"
        exit $EXIT_CODE
    fi
}

catch docker-compose kill
catch docker-compose rm -f

